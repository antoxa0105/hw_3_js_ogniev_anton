/*
1. Функция позволяет использовать набор действий многократно при этом объявляется один раз; 
2. К аргументам функция применяет действия которые записаны в теле функции. 
*/

const operand1 = +prompt('Enter number1');
let operator = prompt('Enter operand');
const operand2 = +prompt('Enter number2');

function runCalculator(a, b){
    switch (operator) {
    case '+' :
        return a + b;
    case '-' :
        return a - b;
    case '*' :
        return a * b;
    case '/':
        return a / b;
    } 
}

console.log(runCalculator(operand1, operand2));